import { StatusBar } from 'expo-status-bar'
import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Button } from './components/welcome_screen'
import Location from './components/Location'
import Calendar from './components/Calendar/Calendar'
import DeliveryPreference from './components/DeliveryPreference/DeliveryPreference'

export default class App extends React.Component {
  render(){
    return (
      <View style={styles.container}>
        {/* <Button /> */}
        {/* <Location /> */}
        {/* <Calendar /> */}
        <DeliveryPreference />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
})
