import React from 'react'
import { StyleSheet, View, Text, ImageBackground, ScrollView } from 'react-native'
import CalendarHeader from './CalendarHeader'
import MiddleSection from './MiddleSection'
import Footer from './Footer'

export default function Calendar () {
    return (
       <View style={styles.parentView}>
       {/* <ScrollView> */}
            <CalendarHeader />
            <MiddleSection />
            <Footer />
        {/* </ScrollView> */}
        </View>
    )
}

const styles = StyleSheet.create({
    parentView : {
        flex : 1
    }
})
