import React from 'react'
import { StyleSheet, View, Text, ImageBackground } from 'react-native'

export default function CalendarHeader () {
    return (
        
            <ImageBackground source={require('../../assets/splash.jpg')} style={styles.header}>
                <View style={styles.headerTextView}>
                    <Text style={styles.headerText}>Cow Milk</Text>
                    <Text style={styles.headerText2}>iOrganic Farm Fresh Of  </Text>
                    <Text style={styles.headerText2}>Green pastures, shady </Text>
                    <Text style={styles.headerText2}>trees and a playing flute</Text>
                </View>
            </ImageBackground>
    )
}

const styles = StyleSheet.create({
    header: {
        width: '106%',
        height: '50%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop : 40,
        marginLeft : -20
      },
      headerTextView : {
        flex : 1,
        // justifyContent : "flex-end",
        // alignItems : 'flex-end',
        alignSelf: 'flex-start',
        marginLeft : 260,
        marginTop : 20
      },
      headerText : {
          fontWeight : 'bold',
          fontSize : 25,
          marginBottom: 8
      },
      headerText2 : {
        fontSize : 13,
      }
})
