import React, { Component } from 'react'
import { StyleSheet, View, Text, ImageBackground, Image } from 'react-native'
// import Constants from 'expo-constants'
import CheckBox from 'react-native-check-box'
import { Calendar } from 'react-native-calendars'

export default class Footer extends Component {

    state = { isChecked1 : false, isChecked2 :false }

render () {
    return (
        
            <View style={styles.header}>
                <View style={styles.headerTextView}>
                    {/* <Text style={styles.headerText2}>Set Order Days</Text> */}
                    <Text style={styles.headerText2}><Text style={styles.headerText}>Set order days</Text></Text>
                    <View style={styles.border}></View>
                </View>
                {/* <View style={styles.imageContainer}> */}
                {/* <Image style={styles.image} className="bgImage" source={require('../../assets/calendar-icon.png')}/> */}
                {/* </View> */}


                <View style={styles.outerView}>
                <CheckBox
    style={{flex: 1, padding: 10}}
    onClick={()=>{
      this.setState({
          isChecked1: true, isChecked2 : false
      })
    }}
    isChecked={this.state.isChecked1}
    rightText={"Run Everyday"}
/><Text></Text>
  <View style={styles.innerView}>
  <CheckBox
    style={{flex: 1, padding: 10}}
    onClick={()=>{
      this.setState({
          isChecked1:false, isChecked2 : true
      })
    }}
    isChecked={this.state.isChecked2}
    rightText={"Run Monday to Friday"}
/><Text>Run Monday to Friday aaaa aaa</Text>
    {/* <Text style={{marginTop: 5}}> this is checkbox</Text> */}
  </View>
</View>
            
            
<View style={styles.calendar}>
            <Calendar
  // Specify style for calendar container element. Default = {}
  style={{
    borderWidth: .0,
    borderColor: 'gray',
    height: '50%',
    width : '180%',
    marginLeft : -70,
    marginBottom : -60,
    marginTop : -40
    // paddingBottom : 55
    
  }}
  // Specify theme properties to override specific styles for calendar parts. Default = {}
  theme={{
    lineHeight : 1,
    backgroundColor: '#ffffff',
    calendarBackground: '#ffffff',
    textSectionTitleColor: '#b6c1cd',
    textSectionTitleDisabledColor: '#d9e1e8',
    selectedDayBackgroundColor: '#00adf5',
    selectedDayTextColor: '#ffffff',
    todayTextColor: '#00adf5',
    dayTextColor: '#2d4150',
    textDisabledColor: '#d9e1e8',
    dotColor: '#00adf5',
    selectedDotColor: '#ffffff',
    arrowColor: 'orange',
    disabledArrowColor: '#d9e1e8',
    monthTextColor: 'blue',
    indicatorColor: 'blue',
    textDayFontFamily: 'monospace',
    textMonthFontFamily: 'monospace',
    textDayHeaderFontFamily: 'monospace',
    textDayFontWeight: '300',
    textMonthFontWeight: 'bold',
    textDayHeaderFontWeight: '300',
    textDayFontSize: 16,
    textMonthFontSize: 16,
    textDayHeaderFontSize: 16
  }}
/>

</View>
        </View>
    )
}}

const styles = StyleSheet.create({
  calendar : {
    marginTop : -270,
  },
    outerView : {
        // flexDirection : 'column',
        marginTop : 50,
        marginLeft : -130
    },
    innerView : {
        // flexDirection : 'row',
        marginTop : -15,
    },
    header: {
        width: '106%',
        height: '52%',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop : -45,
        marginLeft : -20,
        backgroundColor : '#fff',
        
      },
      headerTextView : {
        flex : 1,
        // justifyContent : "flex-end",
        // alignItems : 'flex-end',
        alignSelf: 'flex-start',
        marginLeft : 40,
        marginTop : 75,
        paddingBottom : 10,
        flexDirection: 'column',
        // borderBottomWidth : 1,
        // borderBottomLeftRadius : 10
      },
      border : {
        borderWidth : 0.2,
        marginLeft : -15,
        marginTop : 10,
        width : 400
      },
      headerText : {
          fontWeight : 'bold',
          fontSize : 25,
          marginBottom: 8,
          
      },
      headerText2 : {
        fontSize : 13,
        // borderBottomWidth : 1,
        
      },
      image : {
        height : 60,
        width : 60,
        marginRight : 35
      },
      imageContainer : {
        // marginLeft : 0
      }
})
