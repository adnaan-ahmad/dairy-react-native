import React from 'react'
import { StyleSheet, View, Text, ImageBackground, Image } from 'react-native'

export default function MiddleSection () {
    return (
        
            <View style={styles.header}>
                <View style={styles.headerTextView}>
                    <Text style={styles.headerText2}>Price</Text>
                    <Text style={styles.headerText2}><Text style={styles.headerText}>Rs 1099</Text> for 30 Days   </Text>
                    
                </View>
                {/* <View style={styles.imageContainer}> */}
                <Image style={styles.image} className="bgImage" source={require('../../assets/calendar-icon.png')}/>
                {/* </View> */}
            </View>
    )
}

const styles = StyleSheet.create({
    header: {
        width: '106%',
        height: '22%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop : -260,
        marginLeft : -20,
        backgroundColor : '#eee'
      },
      headerTextView : {
        flex : 1,
        // justifyContent : "flex-end",
        // alignItems : 'flex-end',
        alignSelf: 'flex-start',
        marginLeft : 50,
        marginTop : 40
      },
      headerText : {
          fontWeight : 'bold',
          fontSize : 25,
          marginBottom: 8
      },
      headerText2 : {
        fontSize : 13,
      },
      image : {
        height : 60,
        width : 60,
        marginRight : 35,
        marginTop : -50
      },
      imageContainer : {
        // marginLeft : 0
      }
})
