import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, ScrollView } from 'react-native'

export default function Continue () {

    const pressHandler = () => {
        // console.log('Continue')
    }

    return (
       <View style={styles.container}>
            <TouchableOpacity onPress={pressHandler}><Text style={styles.text}>Continue</Text></TouchableOpacity>    
       </View>
    )
}

const styles = StyleSheet.create({
    // container : {
    //     // flex : 1,
    //     justifyContent: 'center',
    //     backgroundColor : '#e75480',
    //     width : 340,
    //     height : '10%',        
    //     marginTop : 265,
    //     marginLeft : 10,
    // },
    // text : {
    //     color : 'white',
    //     fontSize : 17,
    //     textAlign : 'center',
    //     paddingTop : 20,
    //     fontWeight : 'bold'
    // }
    container : {
        // flex : 1,
        alignSelf : 'center',
        justifyContent: 'center',
        backgroundColor : '#e75480',
        width : '106%',
        height : '25%',        
        marginBottom : '2%',
        // marginHorizontal : '22%'
    },
    text : {
        color : 'white',
        fontSize : 17,
        textAlign : 'center',
        // paddingTop : '2%',
        fontWeight : 'bold'
    }
})
