import React from 'react'
import { StyleSheet, View, Text, ImageBackground, ScrollView } from 'react-native'
import Header from './Header'
import Continue from './Continue'
import Frequency from './Frequency'
import Start from './Start'
import Time from './Time'

export default function DeliveryPreference () {
    return (
       <View style={styles.container}>
       <View style={styles.header}>
            <Header />
        </View>
        {/* <View stle={styles.middle}> */}
            <Frequency />
            <Start />
            <Time />
            
        {/* </View> */}
        <View style={styles.continue}>
            <Continue />
        </View>
       </View>
    )
}

const styles = StyleSheet.create({
    middle : {
        //flex : 1,
        flexDirection : "column",
        alignItems : 'flex-start'
    },
    container : {
        flex : 1,
        //flexDirection : 'column',
        // alignSelf : 'center',
        justifyContent: 'space-around',
        
    },
    header : {
        //flex : 1,
        // flexDirection : 'column',
        // alignSelf : 'center',
        justifyContent: 'flex-start',
        marginBottom : '-0%'
    },
    continue : {
        //flex : 2,
        // flexDirection : 'column',
        // alignSelf : 'center',
        justifyContent: 'flex-end',
    }
})
