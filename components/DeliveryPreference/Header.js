import React from 'react'
import { StyleSheet, View, Text, ImageBackground, ScrollView } from 'react-native'

export default function Header () {
    return (
       <View style={styles.container}>
            <Text style={styles.text}>Delivery Preferences</Text>    
       </View>
    )
}

const styles = StyleSheet.create({
    container : {
        // flex : 1,
        alignSelf : 'center',
        justifyContent: 'center',
        backgroundColor : '#00008b',
        width : '247%',
        height : '25%',        
        marginTop : '14%',
    },
    text : {
        color : 'white',
        fontSize : 17,
        textAlign : 'center',
        // paddingTop : '2%',
        fontWeight : 'bold'
    }
})
