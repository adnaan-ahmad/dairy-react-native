import React, { useState } from 'react'
import { StyleSheet, View, Text, ImageBackground, ScrollView } from 'react-native'
import DatePicker from 'react-native-datepicker'

export default function Start () {

var today = new Date()
var minDate = today.getDate() + "/"+ parseInt(today.getMonth()+1) +"/"+ today.getFullYear()
var maxDate = today.getDate() + "/"+ parseInt(today.getMonth()+1) +"/"+ parseInt(today.getFullYear()+1)
// console.log(maxDate)

const [date, setDate] = useState(minDate)

    return (
       <View style={styles.container}>
            {/* <Text style={styles.text}>Delivery Preferences</Text>    */}
            <View style={styles.headerTextView}>
                    {/* <Text style={styles.headerText2}>Set Order Days</Text> */}
                    <Text style={styles.headerText2}><Text style={styles.headerText}>From when you want to start delivery?</Text></Text>
                    
                    <DatePicker
          style={styles.datePickerStyle}
          date={date} //initial date from state
          mode="date" //The enum of date, datetime and time
          placeholder="Select Date"
          format="DD/MM/YYYY"
          minDate={minDate}
          maxDate={maxDate}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              //display: 'none',
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0,
            },
            dateInput: {
              marginLeft: 36,
            },
          }}
          onDateChange={(date) => {
            setDate(date);
          }}
        />

                    <View style={styles.border}></View>
                </View> 
       </View>
    )
}

const styles = StyleSheet.create({
    
    datePickerStyle: {
        width: 200,
        marginTop: 20,
      },
    container : {
        // flex : 1,
        // backgroundColor : '#00008b',
        width : '95%',
        height : '10%',        
        marginLeft : '-5%',
        marginBottom : '6%',
    },
    text : {
        color : 'white',
        fontSize : 17,
        textAlign : 'center',
        //paddingTop : 20,
        fontWeight : 'bold'
    },
    headerTextView : {
        flex : 1,
        // justifyContent : "flex-end",
        // alignItems : 'flex-end',
        alignSelf: 'flex-start',
        marginLeft : '11%',
        //marginTop : 15,
        //paddingBottom : 10,
        flexDirection: 'column',
        // borderBottomWidth : 1,
        // borderBottomLeftRadius : 10
      },
      headerText2 : {
        //fontSize : 13,
        // borderBottomWidth : 1,
        marginLeft : -20
      },
      headerText : {
        fontWeight: 'bold',
        fontSize : 17,
        //marginBottom: 8,
        
    },
    border : {
        borderWidth : 0.4,
        marginLeft : -20,
        marginTop : '10%',
        width : 320
      },
})
