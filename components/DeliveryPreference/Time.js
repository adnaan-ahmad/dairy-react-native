import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Text, ImageBackground, ScrollView, TouchableOpacity } from 'react-native'

export default function Time () {

  const [frequency, setFrequency] = useState('')        

    const pressHandler1 = () => {
      setFrequency('am')
    }

    const pressHandler2 = () => {
      setFrequency('pm')
    }

    return (
       <View style={styles.container}>
            {/* <Text style={styles.text}>Delivery Preferences</Text>    */}
            <View style={styles.headerTextView}>
                    {/* <Text style={styles.headerText2}>Set Order Days</Text> */}
                    <Text style={styles.headerText2}><Text style={styles.headerText}>Select delivery time</Text></Text>
                    
                    
                    <View style={styles.buttonContainer}>
                    <TouchableOpacity onPress={pressHandler1}>
  <View style={frequency !== 'am' ? styles.circleGradient : styles.circleGradient2}>
    <Text style={frequency !== 'am' ? styles.visit : styles.visit2}>5:00 AM - 7:00 AM</Text>
  </View>
  
 
</TouchableOpacity>
{/* <TouchableOpacity onPress={() => {}}>
<View style={styles.circleGradient}>
    <Text style={styles.visit}>Weekdays</Text>
  </View>
</TouchableOpacity> */}
<TouchableOpacity onPress={pressHandler2}>
  <View style={frequency !== 'pm' ? styles.circleGradient : styles.circleGradient2}>
    <Text style={frequency !== 'pm' ? styles.visit : styles.visit2}>5:00 PM - 7:00 PM</Text>
  </View>
  </TouchableOpacity>

  </View>
                    
                    <View style={styles.border}></View>
                </View> 
       </View>
    )
}

const styles = StyleSheet.create({
    // container : {
    //     // flex : 1,
    //     // backgroundColor : '#00008b',
    //     width : 360,
    //     height : '10%',        
    //     marginTop : 5,
    // },
    // text : {
    //     color : 'white',
    //     fontSize : 17,
    //     textAlign : 'center',
    //     paddingTop : 20,
    //     fontWeight : 'bold'
    // },
    // headerTextView : {
    //     flex : 1,
    //     // justifyContent : "flex-end",
    //     // alignItems : 'flex-end',
    //     alignSelf: 'flex-start',
    //     marginLeft : 40,
    //     marginTop : 15,
    //     paddingBottom : 10,
    //     flexDirection: 'column',
    //     // borderBottomWidth : 1,
    //     // borderBottomLeftRadius : 10
    //   },
    //   headerText2 : {
    //     fontSize : 13,
    //     // borderBottomWidth : 1,
    //     marginLeft : -20
    //   },
    //   headerText : {
    //     fontWeight: 'bold',
    //     fontSize : 17,
    //     marginBottom: 8,
        
    // },
    // border : {
    //     borderWidth : 0.34,
    //     marginLeft : -20,
    //     marginTop : 10,
    //     width : 320
    //   },
    buttonContainer : {
        flexDirection : 'row',
        justifyContent : 'space-around',
        marginLeft : '-14%',
        marginTop : '4%',
        marginBottom : '-5%'
    },
    circleGradient: {
        backgroundColor: "#ccc",
        borderRadius: 5,
        width : '115%',
        // height : 10,
        // paddingVertical : '10%'
        
      },
      circleGradient2: {
        backgroundColor: "#e75480",
        borderRadius: 5,
        width : '115%',
        // height : 10,
        // paddingVertical : '10%'
        
      },
      visit: {
        margin: 2,
        paddingVertical: 7,
        textAlign: "center",
        backgroundColor: "white",
        color: '#888',
        fontSize: 15
      },
      visit2: {
        margin: 2,
        paddingVertical: 7,
        textAlign: "center",
        backgroundColor: "white",
        color: '#e75480',
        fontSize: 15
      },
    container : {
        // flex : 1,
        // backgroundColor : '#00008b',
        width : '95%',
        height : '10%',        
        marginLeft : '-5%',
        marginTop : '10%',
    },
    text : {
        color : 'white',
        fontSize : 17,
        textAlign : 'center',
        //paddingTop : 20,
        fontWeight : 'bold'
    },
    headerTextView : {
        flex : 1,
        // justifyContent : "flex-end",
        // alignItems : 'flex-end',
        alignSelf: 'flex-start',
        marginLeft : '11%',
        //marginTop : 15,
        //paddingBottom : 10,
        flexDirection: 'column',
        // borderBottomWidth : 1,
        // borderBottomLeftRadius : 10
      },
      headerText2 : {
        //fontSize : 13,
        // borderBottomWidth : 1,
        marginLeft : -20
      },
      headerText : {
        fontWeight: 'bold',
        fontSize : 17,
        //marginBottom: 8,
        
    },
    border : {
        borderWidth : 0.1,
        marginLeft : -20,
        marginTop : '10%',
        width : 320
      },
})
