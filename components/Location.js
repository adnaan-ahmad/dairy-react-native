import React from 'react'
import { StyleSheet, Text, View, Image, Button } from 'react-native'
// import styles from '../assets/css/main.js'

export default function Location () {
  return(
    <View style={styles.container}>
        {/* <View> */}
        <Image style={styles.image} className="bgImage" source={require('../assets/delivery.jpeg')}/>
        <Text style={styles.heading}>Delivery Location</Text>
        <Text style={styles.text1}>Set your Delivery Location to Browse</Text>
        <Text style={styles.text2}>dairy around you</Text>
        {/* </View> */}
      <View style={styles.bottom}>
        <Button style={styles.button} padding='20' fontWeight='bold' color='orange' title='         Detect my location         ' />
        
      </View>
      <Text style={styles.text3}>Set Location Manually</Text>
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    image: {
        marginTop:-30,
    },
    heading: {
        fontWeight:'bold',
        fontSize:19,
        marginBottom:12,
    },
    text1:{
        marginBottom:8,
        textAlign:'center',
        fontSize: 15
    },
    text2: {
        marginBottom:50,
        fontSize: 15
    },
    button: {
        marginBottom:-80,
        padding:100,
        fontWeight:'bold',
        width:80
    },
    bottom: {
        marginTop:100,
        
    },
    text3: {
        marginTop:30,
        color: 'powderblue',
        fontSize: 17
    }   
  })
